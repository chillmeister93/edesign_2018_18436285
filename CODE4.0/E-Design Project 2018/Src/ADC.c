/*
 * ADC.c
 *
 *  Created on: 08 Mar 2018
 *      Author: 18436285
 */
#include "Global.h"
#include "ADC.h"

uint16_t ADCValueAmp[20];
uint32_t ADCValueVolt[20];
uint16_t ADCValueTAmb;
uint16_t ADCValueTAmbOut;
uint16_t ADCValueTW;
uint16_t ADCValueTWOut;
int amp[20];
int volt[20];
int RMSAmp = 0;
int RMSVolt = 0;
uint8_t RMS1[4];
uint8_t RMS2[6];
int aveAmp = 0;
int aveAmp1 = 0;
int i = 0;
int aveVolt = 0;
int aveVolt1 = 0;
int j = 0;
int ctr = 0;

void ADC(void)
{
	ADC_ChannelConfTypeDef chdef;


	if(1 == msecFlag)
	{

//		//sample Amp
//
		chdef.Channel = ADC_CHANNEL_12;
		chdef.Rank = 1;
		chdef.SingleDiff = ADC_SINGLE_ENDED;
		chdef.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
		chdef.OffsetNumber = ADC_OFFSET_NONE;
		chdef.Offset = 0;
		HAL_ADC_ConfigChannel(&hadc1, &chdef);

		HAL_ADC_Start(&hadc1);

		while(HAL_ADC_PollForConversion(&hadc1, 1)!=0){
			;
		}

		ADCValueAmp[ctr] = HAL_ADC_GetValue (&hadc1);

		//sample Volt

		chdef.Channel = ADC_CHANNEL_13;
		chdef.Rank = 1;
		chdef.SingleDiff = ADC_SINGLE_ENDED;
		chdef.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
		chdef.OffsetNumber = ADC_OFFSET_NONE;
		chdef.Offset = 0;
		HAL_ADC_ConfigChannel(&hadc1, &chdef);

		HAL_ADC_Start(&hadc1);

		while(HAL_ADC_PollForConversion(&hadc1, 1)!=0){
			;
		}

		ADCValueVolt[ctr] = HAL_ADC_GetValue (&hadc1);

		//sample Tamb

		chdef.Channel = ADC_CHANNEL_8;
		chdef.Rank = 1;
		chdef.SingleDiff = ADC_SINGLE_ENDED;
		chdef.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
		chdef.OffsetNumber = ADC_OFFSET_NONE;
		chdef.Offset = 0;
		HAL_ADC_ConfigChannel(&hadc1, &chdef);


		HAL_ADC_Start(&hadc1);

		while(HAL_ADC_PollForConversion(&hadc1, 1)!=0){
			;
		}

		ADCValueTAmb = HAL_ADC_GetValue (&hadc1);

		//sample Twater

		chdef.Channel = ADC_CHANNEL_9;
		chdef.Rank = 1;
		chdef.SingleDiff = ADC_SINGLE_ENDED;
		chdef.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
		chdef.OffsetNumber = ADC_OFFSET_NONE;
		chdef.Offset = 0;
		HAL_ADC_ConfigChannel(&hadc1, &chdef);


		HAL_ADC_Start(&hadc1);

		while(HAL_ADC_PollForConversion(&hadc1, 1)!=0){
			;
		}

		ADCValueTW = HAL_ADC_GetValue (&hadc1);


		// now use the values

		//Amp and Volt Conversion

		aveAmp = aveAmp + ADCValueAmp[ctr];

		aveVolt = aveVolt + ADCValueVolt[ctr];

		ctr++;

		if(ctr > 19)
		{
			aveAmp1 = aveAmp/20;

			aveVolt1 = aveVolt/20;

			ctr = 0;
			aveAmp = 0;
			aveVolt = 0;

			for(i = 0; i < 20; i++)
			{
				amp[i] = (ADCValueAmp[i] - aveAmp1)*13*1000/1500;
				RMSAmp = RMSAmp + (amp[i]*amp[i]);

				volt[i] = (ADCValueVolt[i] - aveVolt1)*(220*1000);//*22*1000/1500;
				RMSVolt = RMSVolt + (volt[i]*volt[i]);
			}

			RMSAmp = sqrt(RMSAmp/20);

			RMSVolt = sqrt(RMSVolt/(20*1500));

			RMS1[0] = (RMSAmp/1000)%10 + 48;
			RMS1[1] = (RMSAmp/100)%10 + 48;
			RMS1[2] = (RMSAmp/10)%10 + 48;
			RMS1[3] = (RMSAmp%10) + 48;

			RMS2[0] = (RMSVolt/1000)%10 + 48;
			RMS2[1] = (RMSVolt/100)%10 + 48;
			RMS2[2] = (RMSVolt/10)%10 + 48;
			RMS2[3] = (RMSVolt/1)%10 + 48;
			RMS2[4] = 0 + 48;
			RMS2[5] = 0 + 48;
		}

		//Tamb Conversion

		ADCValueTAmbOut = ((ADCValueTAmb*3300/4096) - 500)/10;

		//TW Conversion

		ADCValueTWOut = ((ADCValueTW*3300/4096) - 500)/10;

		msecFlag = 0;
	}



}
