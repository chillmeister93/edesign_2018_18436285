/*
 * User.c
 *
 *  Created on: 02 Mar 2018
 *      Author: 18436285
 */

#include "User.h"
#include "Global.h"

uint8_t stdnum[13] = "$A,18436285\r\n";

char rx[40];		//complete string from UART

uint8_t rx_1[1];	//single incoming byte from UART

uint8_t set_temp1[1];	//single digit temp

uint8_t set_temp2[2];	//double digit temp

uint8_t set_temp3[8] = "100";	//triple digit temp capped at 100

int valvepos;
char valveopen[4];
char valveclosed[6];

int heatersw;
char heateron[2];
char heateroff[3];

char ADCValueTAmbOuttest[10];
char ADCValueTWOuttest[10];

//char ADCValueTAmbOut1;

#define MAXTXLEN  50   // maximum length of transmit buffer (replies sent back to UART)

char txbuffer[MAXTXLEN];     	// buffer for replies that are to be sent out on UART

uint8_t numcharswritten;

uint8_t Int2String(char* output_string, int16_t val, uint8_t maxlen);


int flag = 0;

int count = 0;

int temp_length = 0;

void HAL_UART_RxCpltCallback (UART_HandleTypeDef*huart)
{
	flag = 1;
}

void MainProcess(void)
{
	  HAL_UART_Receive_IT(&huart1, rx_1, 1);

	  LED();

	  ADC();

	  if(1 == flag)
	  {

		  flag = 0;

		  if('$' == rx_1[0])
		  {
			  count = 0;
		  }

	  		rx[count] = rx_1[0];
	  		count++;

	  		 if(0x0A == rx_1[0])
	  			  {

					 switch(rx[1])
					 {

					 case 'A':
						HAL_UART_Transmit_IT(&huart1, stdnum, sizeof(stdnum));
						break;

					 case 'B':

						 if('1' == rx[3])
						 {
						 HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_SET);

						 valvepos = 1;
						 }

						 else if('0' == rx[3])
						 {
						 HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_RESET);

						 valvepos = 0;
						 }

						 HAL_UART_Transmit_IT(&huart1, "$B\r\n", 4);
						break;

					 case 'D':

						 if('1' == rx[3])
						 {
						 HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);

						 heatersw = 1;
						 }

						 else if('0' == rx[3])
						 {
						 HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);

						 heatersw = 0;
						 }

						 HAL_UART_Transmit_IT(&huart1, "$D\r\n", 4);
						break;

					 case 'F':
							temp_length = count;

//							if(6 == temp_length)
//							{
//								set_temp1[0] = rx[3];
//							}
//
//							else if(7 == temp_length)
//							{
//								set_temp2[0] = rx[3];
//								set_temp2[1] = rx[4];
//							}

							switch(temp_length)
							{

							case 6:

								if(isdigit(rx[3]))
								{
									set_temp1[0] = rx[3];
								}

								else
								{
									HAL_UART_Transmit_IT(&huart1, "Invalid Command\r\n", 17);
								}
								break;

							case 7:

								if(isdigit(rx[3]) && isdigit(rx[4]))
								{
									set_temp2[0] = rx[3];
									set_temp2[1] = rx[4];
								}

								else
								{
									HAL_UART_Transmit_IT(&huart1, "Invalid Command\r\n", 17);
								}

								break;

							case 8:

								if(isdigit(rx[3]) && isdigit(rx[4]) && isdigit(rx[5]))
								{

								}

								else
								{
									HAL_UART_Transmit_IT(&huart1, "Invalid Command\r\n", 17);
								}
								break;

							default:

								HAL_UART_Transmit_IT(&huart1, "Invalid Command\r\n", 17);
							}

						HAL_UART_Transmit_IT(&huart1, "$F\r\n", 4);
						break;

					 case 'G':
						HAL_UART_Transmit(&huart1, "$G,", 3, 1);

//						if(6 == temp_length)
//						{
//							HAL_UART_Transmit(&huart1, set_temp1, 1, 1);
//						}
//
//						else if(7 == temp_length)
//						{
//							HAL_UART_Transmit(&huart1, set_temp2, 2, 1);
//						}
//
//						else if(8 == temp_length)
//						{
//							HAL_UART_Transmit(&huart1, set_temp3, 3, 1);
//						}

						switch(temp_length)
						{

						case 6:
							HAL_UART_Transmit(&huart1, set_temp1, 1, 1);
							break;

						case 7:
							HAL_UART_Transmit(&huart1, set_temp2, 2, 1);
							break;

						case 8:
							HAL_UART_Transmit(&huart1, set_temp3, 3, 1);
							break;
						}

						HAL_UART_Transmit_IT(&huart1, "\r\n", 2);
						break;

					case 'K':

						valveopen[0] = 'O';
						valveopen[1] = 'P';
						valveopen[2] = 'E';
						valveopen[3] = 'N';

						valveclosed[0] = 'C';
						valveclosed[1] = 'L';
						valveclosed[2] = 'O';
						valveclosed[3] = 'S';
						valveclosed[4] = 'E';
						valveclosed[5] = 'D';

						heateron[0] = 'O';
						heateron[1] = 'N';

						heateroff[0] = 'O';
						heateroff[1] = 'F';
						heateroff[2] = 'F';


						HAL_UART_Transmit(&huart1, "$K,", 3, 1);

						HAL_UART_Transmit(&huart1, RMS1, 4, 1);	//Output Current RMS

						HAL_UART_Transmit(&huart1, ",0,", 3, 1);

//						HAL_UART_Transmit(&huart1, RMS2, 6, 1);	//Output Voltage RMS
//
//						HAL_UART_Transmit(&huart1, ",", 1, 1);

						Int2String(ADCValueTAmbOuttest, ADCValueTAmbOut, 10);	//Convert Ambient Temp to string

						HAL_UART_Transmit(&huart1, ADCValueTAmbOuttest, 2, 1);	//Output Ambient Temp

						HAL_UART_Transmit(&huart1, ",", 1, 1);

						Int2String(ADCValueTWOuttest, ADCValueTWOut, 10);	//Convert Water Temp

						HAL_UART_Transmit(&huart1, ADCValueTWOuttest, 2, 1);	//Output Water Temp

						HAL_UART_Transmit(&huart1, ",0,", 3, 1);	//Output Flow Rate

						if(1 == heatersw)
						{
							HAL_UART_Transmit(&huart1, heateron, 2, 1);
						}

						else if(0 == heatersw)
						{
							HAL_UART_Transmit(&huart1, heateroff, 3, 1);
						}

						HAL_UART_Transmit(&huart1, ",", 1, 1);

						if(1 == valvepos)
						{
							HAL_UART_Transmit(&huart1, valveopen, 4, 1);
						}

						else if(0 == valvepos)
						{
							HAL_UART_Transmit(&huart1, valveclosed, 6, 1);
						}

						HAL_UART_Transmit_IT(&huart1, "\r\n", 2);

						break;

					 default:
						 HAL_UART_Transmit_IT(&huart1, "Invalid Command\r\n", 17);
					 }

	  			  }
	  }
}

//BEGIN USED CODE FROM DEMO 2
// String2Int - convert ASCII string to integer variable
uint8_t String2Int(char* input_string, int16_t* output_integer)
{
    int retval = 0;

    if (*input_string == '\0')
        return 0;

    int sign = 1;
    if (*input_string == '-')
    {
        sign = -1;
        input_string++;
    }

    while ((*input_string >= '0') && (*input_string <= '9'))
    {
        retval *= 10;
        retval += (*input_string - 48);

        if (((sign == 1) && (retval >= 32768)) ||
            ((sign == -1) && (retval >= 32769)))
            return 0;

        input_string++;
    }
    *output_integer = (int16_t)(sign * retval);
    return 1;
}

// convert integer var to ASCII string
uint8_t Int2String(char* output_string, int16_t val, uint8_t maxlen)
{
	if (maxlen == 0)
		return 0;

	int numwritten = 0;

	if (val < 0)
	{
		output_string[0] = '-';
		output_string++;
		maxlen--;
		val = -val;
		numwritten = 1;
	}

	uint8_t digits = 0;
	if (val < 10)
		digits = 1;
	else if (val < 100)
		digits = 2;
	else if (val < 1000)
		digits = 3;
	else if (val < 10000)
		digits = 4;
	else
		digits = 5;

	if (digits > maxlen)
		return 0; // error - not enough space in output string!

	int writepos = digits;
	while (writepos > 0)
	{
		output_string[writepos-1] = (char) ((val % 10) + 48);
		val /= 10;
		writepos--;
		numwritten++;
	}

	return numwritten;
}
//END USED CODE FROM DEMO 2
