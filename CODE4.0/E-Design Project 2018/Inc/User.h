/*
 * User.h
 *
 *  Created on: 02 Mar 2018
 *      Author: 18436285
 */

#ifndef USER_H_
#define USER_H_

#include "main.h"
#include "stm32f3xx_hal.h"
#include "stdbool.h"
#include "ctype.h"

extern uint8_t rx_1[1];	//single incoming byte from UART

extern uint8_t set_temp1[1];	//single digit temp

extern uint8_t set_temp2[2];	//double digit temp

extern uint8_t set_temp3[8];	//triple digit temp capped at 100

extern int temp_length;

UART_HandleTypeDef huart1;

void MainProcess(void);

#endif /* USER_H_ */


