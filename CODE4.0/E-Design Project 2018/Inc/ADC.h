/*
 * ADC.h
 *
 *  Created on: 08 Mar 2018
 *      Author: 18436285
 */

#ifndef ADC_H_
#define ADC_H_

#include "main.h"
#include "stm32f3xx_hal.h"
#include "stdbool.h"
#include "LED.h"

extern uint8_t RMS1[4];
extern uint8_t RMS2[6];
extern uint16_t ADCValueTAmbOut;
extern uint16_t ADCValueTWOut;

//extern uint8_t ADCValueTAmbOut1;
//extern uint8_t ADCValueTAmb2;
//extern uint8_t ADCValueTAmb1;

void ADC(void);

#endif /* ADC_H_ */
