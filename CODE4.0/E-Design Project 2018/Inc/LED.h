/*
 * LED.h
 *
 *  Created on: 02 Mar 2018
 *      Author: 18436285
 */

#ifndef LED_H_
#define LED_H_

#include "main.h"
#include "User.h"
#include "stm32f3xx_hal.h"

extern int msecCntr;
extern int msecFlag;

void HAL_SYSTICK_Callback(void);

void LED(void);

#endif /* LED_H_ */
